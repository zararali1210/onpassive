/**
 * @swagger
 * /api/user/login:
 *   post:
 *     tags:
 *       - Auth
 *     name: Login
 *     summary: "Login User by Email and Password"
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       'Notes':
 *         description: Login
 *       '200':
 *         description: Verify successfully
 *       '401':
 *         description: Port id, not found in db
 *       '403':
 *         description: Not Found
 */


/**
 * @swagger
 * /api/user/createUser:
 *   post:
 *     tags:
 *       - User
 *     name: Login
 *     summary: "Create User"
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             first_name:
 *               type: string
 *             last_name:
 *               type: string
 *             email:
 *               type: string
 *             password:
 *                type: string
 *     responses:
 *       'Notes':
 *         description: You have to pass only 3 parameters For Login type,password and(email OR Phone)
 *       '200':
 *         description: Verify successfully
 *       '401':
 *         description: OnPassive id, not found in db
 *       '403':
 *         description: Not Found
 */


 /**
 * @swagger
 * /api/user/getUserLiting:
 *   get:
 *     tags:
 *       - User
 *     summary: "Get All Users Listing"
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: keyword
 *         schema:
 *           type: string
 *       - in: query
 *         name: pageNumber
 *         schema:
 *           type: number
 *     responses:
 *       '401':
 *         description: OnPassive id, not found in db
 *       '403':
 *         description: Not Found
 */

 
 /**
 * @swagger
 * /api/user/findOne:
 *   get:
 *     tags:
 *       - User
 *     summary: "Get details by id"
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: number
 *     responses:
 *       '401':
 *         description: OnPassive id, not found in db
 *       '403':
 *         description: Not Found
 */


 
 /**
 * @swagger
 * /api/user/deleteOne:
 *   delete:
 *     tags:
 *       - User
 *     summary: "Delete by id"
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: id
 *         schema:
 *           type: number
 *     responses:
 *       '401':
 *         description: OnPassive id, not found in db
 *       '403':
 *         description: Not Found
 */
