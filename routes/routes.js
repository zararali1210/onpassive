const usersRoute = require('./user.route');

module.exports = (app) => {
    app.use('/api/user/', usersRoute);
};
