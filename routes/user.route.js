const express = require('express');
const router = express.Router();
const passport = require('passport');
const usersController = require('../controllers/users.controller');
const accessTokenMiddleware = require('../middleware/access_token')
require('./../middleware/passport')(passport);
const authorizationRequired = accessTokenMiddleware.authorizationRequired  // Use to  Authorized User
const accessToken = accessTokenMiddleware.accessToken  // Check if User login in different device


router.post('/login', usersController.login);
router.post('/createUser',usersController.createUser);
router.get('/getUserLiting',authorizationRequired,accessToken,usersController.getUserListing);
router.get('/findOne', authorizationRequired,accessToken,usersController.getOne);
router.delete('/deleteOne',authorizationRequired,accessToken, usersController.deleteOne);

module.exports = router;