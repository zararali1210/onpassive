const {ExtractJwt, Strategy} = require('passport-jwt');
const User = require('../models/model').User;
const CONFIG = require('../config/config');
const {to} = require('../services/util.service');

module.exports = function (passport) {
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption;
    passport.use(new Strategy(opts, async function (jwt_payload, done) {
        let err, user;
        [err, user] = await to(User.findByPk(jwt_payload.uid));
        if (err) return done(err, false);
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
        }
    }));
};
