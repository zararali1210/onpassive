const Models = require("../models/model");
const {to,ReE, TE} = require('../services/util.service');
const passport = require('passport');
require('./../middleware/passport')(passport);

   exports.accessToken = async (req, res, next) => {
       await commonForAccessToken(req, res, next);
   }

   exports.authorizationRequired = async (req, res, next) => {
       await commonForAuthRequired(req, res, next);
   }


/*-----------------We used for condtion base token on same api;----------------*/

    module.exports.checkConditionForStore = async (req, res, next) => {

        if (req.body.type == 1) {
        next();
        } else {
        await commonForAuthRequired(req,res,next);
        }
    }

    exports.accessTokenForStore = async (req, res, next) => {
    
        let userInfo= req.body;
        if(userInfo.type==1){
        next();
        }else{
        await commonForAccessToken(req, res, next);
        }   
    }

    const commonForAccessToken= async(req,res,next)=>{
        
        let headerToken = req.headers.authorization.toString();
        let [err, userToken] = await to(Models.LoginToken.findOne({
        where: { token: headerToken },
        include: [{ model: Models.User, as: "users"}]
        }));
        if(!userToken){
            res.status(401).json({
                status: 401,
                message: "You are not currently logged in"
            })
        }else{
          return next();
        }
    }

    const commonForAuthRequired= async(req,res,next)=>{
       
        new Promise((resolve, reject) => {
            passport.authenticate('jwt', {
                session: false
            }, (err, user) => {
                if (err) reject(new Error(err))
                else if (!user) {
                    res.status(401).json({
                        status: 401,
                        message: "You are not currently logged in"
                    })
                } else {
                    resolve(user)
                    next();
                }
            })(req, res, next)
        })
    }