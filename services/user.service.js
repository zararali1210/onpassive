const {to, TE} = require('../services/util.service');
const Models = require('../models/model');
const commonService= require('../services/common.service');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const bcrypt = require("bcryptjs");
const bcrypt_p = require('bcrypt-promise');
const jwt = require('jsonwebtoken');

const authUserToLogin = async function (userInfo) {

      let user, err;
      let userQuery = { where: {}};
      userQuery.where.email = userInfo.email;
      [err, user] = await to(Models.User.findOne(userQuery)); 
      if (!user) TE("Not registered");
      await commonForAuthLogin(user,userInfo,userQuery);
      return user;
  };


  const commonForAuthLogin= async(user,userInfo,userQuery)=>{
      
    let err;
    let currentPassword = user.password == "" || user.password == null ? "0" : user.password;
    [err, pass] = await to(bcrypt_p.compare(userInfo.password, currentPassword));
    if (err) TE(err.message);
    if (!pass) TE("Invalid Credentials!");
    [err, user] = await to(Models.User.findOne(userQuery));
    return user;
}


const deleteRecord = async (userInfo) => {

    return await Models.User.destroy({
        where: {
            id: userInfo.id
        }
    });

}

/*Update record by id*/ 

const updateRecord = async (userInfo) => {

    return await Models.User.update(userInfo, {
        where: {
            id: userInfo.id
        }
    });

}

/* Check user already */

const getArtistUser = async (userInfo) => {

    let err, user;
    [err, user] = await to(Models.User.findOne({
    where: {
    email: userInfo.email
    }
    }));
    if (user) TE("User already exist");
    return user;
  
  }

  const generateUniqueId = async (userInfo) => {
    return (
      new Date().getTime().toString(36) +
      "" +
      new Date().getTime().toString() +
      "" +
      new Date().getTime().toString(20)
    );
  };

/*Save New Record*/ 

const saveRecord = async (userInfo) => {

    await getArtistUser(userInfo); // To Check existing user if exist thro error;
    userInfo.uid = await generateUniqueId(); // to generate uid
    userInfo.password = bcrypt.hashSync(userInfo.password); // Bcrypt Password to save table
    console.log(userInfo);
    return await Models.User.create(userInfo);

}



const accessToken = async (token) => {

    let newToken = token.replace('Bearer ', '')
    let decodeData = await jwt.decode(newToken);
    let query = {
    where: {
    user_id: decodeData.uid
    }
    }
    let tokenData = await Models.LoginToken.findOne(query) /*Get user token which is store in token table*/
    let createObj = {
    token: token,
    user_id: decodeData.uid.toString()
    }
    if (!tokenData) {
    return Models.LoginToken.create(createObj)
    } else {
    let body = {
    token: token
    }
    return Models.LoginToken.update(body, query)
    }
  }

/*Get User listing with search by name and pagination also */

const getdata= async(userInfo)=>{

      let err, user, userQuery;
      userQuery = {
      where: {},
      order: [
      ['createdAt', 'DESC']
      ],
      raw: true
      };
      userQuery.where={
      [Op.or]: [{
      first_name: {
      [Op.like]: `%${userInfo.keyword}%`
      },
      },{
      last_name: {
      [Op.like]: `%${userInfo.keyword}%`
      },
      }]
      }
      let limit = 10;
      userQuery.limit = limit;
      let page = userInfo.pageNumber != 0 ? userInfo.pageNumber : 1
      userQuery.offset = (page - 1) * limit;
      [err, user] = await to(Models.User.findAndCountAll(userQuery));
      paginationOutPut = await commonService.paginationOutPut(user.rows, page, limit, user.count);
      if (err) TE(err.message);
      return paginationOutPut;
}

const getOneData= async(userInfo)=>{

if(!userInfo.id)TE("Id is required"); 
 return Models.User.findOne({where:{id:userInfo.id}})

}

const deleteOne= async(userInfo)=>{
    if(!userInfo.id)TE("Id is required");
    return Models.User.destroy({where:{id:userInfo.id}})
}

module.exports={

    deleteRecord,
    updateRecord,
    saveRecord,
    getdata,
    getOneData,
    deleteOne,
    authUserToLogin,
    accessToken
}