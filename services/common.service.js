
/*Common function for Pagination*/

const paginationOutPut = async (currentUser, page, limit, totalCount) => {
    const currentPagination = {
        list: typeof currentUser == "string" ? parseInt(currentUser) : currentUser,
        pageNumber: typeof page == "string" ? parseInt(page) : page,
        pageSize: typeof limit == "string" ? parseInt(limit) : limit,
        totalRecords: typeof totalCount == "string" ? parseInt(totalCount) : totalCount,
    }
    return currentPagination;
}
/**
 * this function is responsible for generating random password having Upper case,
 * lower case and special character
 */
const generatePassword = async () => {
      
    charset1 = "abcdefghijklmnopqrstuvwxyz",
    charset2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    charset3 = '0123456789',
    charset4 = '!@#$%^&*'
    return await Promise.all([genrateCharSet(charset1), genrateCharSet(charset2), genrateCharSet(charset3), genrateCharSet(charset4)]).then(async (result) => {
    let password = "";
    for (let i = 0; i < result.length; i++) {
    password += result[i];
    }
    return password
})
}

const genrateCharSet = async (data) => {
let retVal = '';
for (var i = 0, n = data.length; i < 2; ++i) {
    retVal += data.charAt(Math.floor(Math.random() * n));
}
return retVal;
}

module.exports={
paginationOutPut,
generatePassword
}