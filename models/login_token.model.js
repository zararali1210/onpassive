module.exports = (sequelize, DataTypes) => {
    const LoginToken = sequelize.define("LoginToken", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        user_id: {
            type: DataTypes.STRING,
        },
        token: {
            type: DataTypes.TEXT
        },
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE
        }
    }, {});

    LoginToken.associate = function (models) {
        LoginToken.belongsTo(models.User, { foreignKey: 'user_id', as: 'users' });
    };
    return LoginToken;
};