const bcrypt = require('bcrypt');
const bcrypt_p = require('bcrypt-promise');
const jwt = require('jsonwebtoken');
const CONFIG = require('../config/config');
const {to, TE} = require('../services/util.service');

module.exports = (sequelize, DataTypes) => {
    
    const User = sequelize.define('User', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        uid:{
            allowNull: false,
            primaryKey: true,
            type: DataTypes.STRING
        },
        first_name: {
            allowNull: true,
            type: DataTypes.STRING
        },
        last_name: {
            allowNull: true,
            type: DataTypes.STRING
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING
        },
        email: {
            allowNull: false,
            type: DataTypes.STRING
        },
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE
        }
    }, {});

    User.associate = function (models) {
    }
   
    User.prototype.comparePassword = async function (pw) {
        let err, pass;
        //if (!this.password) TE('password not set');
        if(!this.password) TE('Invalid Credentials!');
        [err, pass] = await to(bcrypt_p.compare(pw, this.password));
        if (err) TE(err);

        //if (!pass) TE('invalid password');
          if(!pass) TE('Invalid Credentials!');
        return this;
    };

    User.prototype.getJWT = function () {

        let expiration_time = parseInt(CONFIG.jwt_expiration);
        return "Bearer " + jwt.sign({id:this.id,uid:this.uid}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
    
    };
  
    User.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };
    return User

};
