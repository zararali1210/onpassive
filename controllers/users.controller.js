const Models = require("../models/model");
const {  to, ReE, ReS, TE } = require("../services/util.service");
const  userService= require('../services/user.service');
const commonService= require('../services/common.service');



const login = async function (req, res) {
    
    let err, user;
    let start = new Date();
    [err, user] = await to(userService.authUserToLogin(req.body));
    if (err) return ReE(res, err, 422);
    let token = user.getJWT(); /*To generate  JWT token*/
    [err, accessTokenData] = await to(userService.accessToken(token)); // Save token in token table
    if (err) return ReE(res, err, 422);
    return ReS(res, {
        message: "Login successful",
        token: token
    },200,start);
};


const createUser = async(req,res)=>{

    const userInfo = req.body;
    let start = new Date();
    [err, data] = await to(userService.saveRecord(userInfo));
    if (err) return ReE(res, err, 200);
    return ReS(res, {
        message: "Created Sucessfully"
    }, 200, start);
}

const getUserListing= async(req,res)=>{
   
    let userInfo = req.query;
    let start= new Date();
    let [err, data] = await to(userService.getdata(userInfo));
    if (err) return ReE(res, err, 200);
    return ReS(res, {
        message: 'Successfully',
        response:data
    }, 200,start);
}

const getOne= async(req,res)=>{

    let userInfo = req.query;
    let start= new Date();
    let [err, data] = await to(userService.getOneData(userInfo));
    if (err) return ReE(res, err, 200);
    return ReS(res, {
        message: 'Successfully',
        response:data
    }, 200,start);
}

const deleteOne= async(req,res)=>{

    let userInfo = req.query;
    let start= new Date();
    let [err, data] = await to(userService.deleteOne(userInfo));
    if (err) return ReE(res, err, 200);
    return ReS(res, {
        message: 'Deleted successfully'
    }, 200,start);

}

module.exports = {
    createUser,
    getUserListing,
    getOne,
    deleteOne,
    login
}